<?php
/**
 * Created by PhpStorm.
 * User: ntstv
 * Date: 09.11.2015
 * Time: 15:29
 */

namespace rshamyan;


/**
 * Class Location
 * @package rshamyan
 */
class Location
{
    /**
     * @var float
     */
    private $x;
    /**
     * @var float
     */
    private $y;
    /**
     * @var float
     */
    private $z;

    /**
     * Returns x
     * @return float
     */
    public function getX() {
        return $this->x;
    }

    /**
     * Sets x position
     * @param float $x
     * @return float
     */
    public function setX($x) {
        return $this->x = $x;
    }

    /**
     * Return y positon
     * @return float
     */
    public function getY() {
        return $this->y;
    }

    /**
     * Sets y position
     * @param float $y
     * @return float
     */
    public function setY($y) {
        return $this->y = $y;
    }

    /**
     * Sets z position
     * @param float $z
     * @return float
     */
    public function setZ($z) {
        return $this->z = $z;
    }

    /**
     * Returns z position
     * @return float
     */
    public function getZ() {
        return $this->z;
    }

    /**
     * Calculates distance to another location
     * @param Location $loc
     * @return float
     */
    public function getDistance(Location $loc) {
        $distance = sqrt(pow($loc->getX() - $this->x, 2) +
            pow($loc->getY() - $this->y, 2) +
            pow($loc->getZ() - $this->z, 2));
        return $distance;
    }
}