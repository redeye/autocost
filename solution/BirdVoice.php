<?php
/**
 * Created by PhpStorm.
 * User: ntstv
 * Date: 09.11.2015
 * Time: 15:58
 */

namespace rshamyan;

/**
 * Class BirdVoice
 * @package rshamyan
 */
abstract class BirdVoice
{
    /**
     * @param string $melody
     * @return string
     */
    public abstract function singMelody(string $melody);
}