<?php
/**
 * Created by PhpStorm.
 * User: ntstv
 * Date: 09.11.2015
 * Time: 16:05
 */

namespace rshamyan;

/**
 * Class CrowVoice
 * @package rshamyan
 */
class CrowVoice extends BirdVoice
{
    /** @override */
    public function singMelody(string $melody)
    {
        return 'Crow: ' . $melody;
    }
}