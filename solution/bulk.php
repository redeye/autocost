<?php
/**
 * Created by PhpStorm.
 * User: ntstv
 * Date: 09.11.2015
 * Time: 16:28
 */

/*
* На вход данному скрипту передаются неизвестные параметры
* Задача: максимально оптимизировать код, чтобы при любых условиях он на 100% сохранил свою функциональность
* Т.е. вывод скрипта до и после оптимизации должен быть одинаковым
* Оптимизация подразумевает собой хороший PHP5 стиль, безопасность, подготовленность к highload и т.д.
* В результате эти несколько строчек должны стать "идеальным" кодом с Вашей точки зрения
*/

// подключение к БД опущено

//$field = 'name';
//$user1 = $HTTP_GET_VARS['user1'];$result = mysql_query('SELECT * FROM users WHERE user='.$user1);
//$user1_data = mysql_fetch_assoc($result);
//$user2 = $HTTP_GET_VARS['user2'];$result = mysql_query('SELECT * FROM users WHERE user='.$user2);
//$user2_data = mysql_fetch_assoc($result);
//$user3 = $HTTP_GET_VARS['user3'];$result = mysql_query('SELECT * FROM users WHERE user='.$user3);
//$user3_data = mysql_fetch_assoc($result);
//$users = array();
//$users[] = $user1_data;
//$users[] = $user2_data;
//$users[] = $user3_data;

//$i = 0;
//do{print($users[$i]["$field"]."<br>");$i++;}while
//($i<3);

class HTTPRequest {
    /**
     * @var string
     */
    private $user1;
    /**
     * @var string
     */
    private $user2;
    /**
     * @var string
     */
    private $user3;

    /**
     * Request constructor
     */
    public function __construct()
    {
        $this->user1 = is_string($_GET['user1']) ? $_GET['user1'] : null;
        $this->user2 = is_string($_GET['user2']) ? $_GET['user2'] : null;
        $this->user3 = is_string($_GET['user3']) ? $_GET['user3'] : null;
    }

    /**
     * Returns array of users
     * @return array
     */
    public function getUsers()
    {
        return [
            'user1' => $this->user1,
            'user2' => $this->user2,
            'user3' => $this->user3
        ];
    }
}

class ActiveQuery {

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var PDOStatement
     */
    protected $stmt;

    /**
     * ActiveQuery constructor.
     * @param PDO $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Prepares statement
     * @param $query
     * @return PDOStatement
     */
    public function prepare($query) {
        return $this->pdo->prepare($query);
    }

    /**
     * Executes statement
     * @param PDOStatement $prepare
     * @param array $args
     * @return bool
     */
    public function execute($prepare, array $args) {
        $this->stmt = $prepare;
        return $prepare->execute($args);
    }
}

class User extends ActiveQuery {
    /**
     * @var string
     */
    private $user;

    private $data;

    /**
     * @param string $user
     * @param PDO $pdo
     * @return null|User
     */
    public static function find($user, $pdo) {
        $res = new User($pdo, $user);
        if ($user) {
            $stmt = $res->prepare('SELECT * FROM users WHERE user = :user LIMIT 1');
            if (!$res->execute($stmt, ['user' => $user])) {
                $res = null;
            }
        }
        return $res;

    }

    /**
     * User constructor.
     * @param PDO $pdo
     * @param string $user
     */
    public function __construct(PDO $pdo, $user)
    {
        parent::__construct($pdo);
        $this->user = $user;
    }

    /**
     * Returns data
     * @return mixed|null
     */
    public function getData() {
        $result = null;
        if (!$this->data) {
            $result = $this->stmt->fetch(PDO::FETCH_ASSOC);
            $this->data = $result;
        }
        else {
            $result = $this->data;
        }
        return $result;
    }

}

//$pdo = new PDO('mysql:host=example.com;dbname=database', 'user', 'password');
$request = new HTTPRequest();
$users = $request->getUsers();
$field = 'name';
foreach(['user1', 'user2', 'user3'] as $userName) {
    $user = User::find($userName, $pdo);
    $print = 'null';
    if ($user && $user->getData()[$field]) {
        $print = $user->getData()[$field];
    }
    echo $print . '<br/>';
}




