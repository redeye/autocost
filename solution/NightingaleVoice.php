<?php
/**
 * Created by PhpStorm.
 * User: ntstv
 * Date: 09.11.2015
 * Time: 16:04
 */

namespace rshamyan;

/**
 * Class NightingaleVoice
 * @package rshamyan
 */
class NightingaleVoice extends BirdVoice
{
    public function singMelody(string $melody)
    {
        return "Nightingale: " . $melody;
    }
}