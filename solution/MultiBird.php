<?php
/**
 * Created by PhpStorm.
 * User: ntstv
 * Date: 09.11.2015
 * Time: 15:25
 */

namespace rshamyan;


use rshamyan\Location;

/**
 * Class MultiBird
 * Can sing as nightingale or crawl
 * @package rshamyan
 */
class MultiBird implements \bird {

    /**
     * max bird fly distance at once
     * At meters
     */
    const MAX_DISTANCE = 100;

    /**
     * @var Location $location
     */
    private $location;

    /**
     * @var string $kind
     */
    private $kind;

    /**
     * @var string
     */
    private $melody;

    /**
     * @var BirdVoice
     */
    private $voice;

    /**
     * Destructs bird
     */
    public function __destruct() {
        $this->killBird();
    }

    public function __clone() {
        $this->location = null;
    }

    /**
     * Sets bird kind
     * @param string $type
     * @throws \Exception
     */
    public function setKind(string $type)
    {
        $factory = new BirdVoiceFactory();
        $this->voice = $factory->buildVoice($type);
        $this->kind = $type;
    }

    /**
     * Returns bird kind
     * @return string
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Return bird location
     * @return \rshamyan\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets bird location
     * @param \rshamyan\Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * Flies to location
     * @param \rshamyan\Location $location
     * @throws \Exception
     */
    public function flyTo(Location $location)
    {
        $distance = $this->location->getDistance($location);
        if ($distance > self::MAX_DISTANCE) {
            throw new \Exception('Too long distance');
        } else {
            $this->location = $location;
        }
    }

    /**
     * Sets melody
     * @param string $melody
     */
    public function setMelody(string $melody)
    {
        $this->melody = $melody;
    }

    /**
     * Sings melody
     * @return string
     */
    public function singMelody()
    {
        return $this->voice->singMelody($this->melody);
    }

    /**
     * Kills bird
     */
    public function killBird()
    {
        $this->kind = null;
        $this->melody = null;
        $this->location = null;
    }


    /**
     * Clones bird
     * @param int $count
     * @return MultiBird[]
     */
    public function cloneBird(int $count)
    {
        $res = [];
        for ($i = 0; $i < $count; $i++) {
            $res[] = clone $this;
        }
        return $res;
    }
}