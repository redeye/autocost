<?php
/**
 * Created by PhpStorm.
 * User: ntstv
 * Date: 09.11.2015
 * Time: 15:57
 */

namespace rshamyan;

/**
 * Class BirdVoiceFactory
 * @package rshamyan
 */
class BirdVoiceFactory
{
    /**
     * @param $type
     * @return null|CrowVoice|NightingaleVoice
     * @throws \Exception
     */
    public function buildVoice($type) {
        $res = null;
        switch($type) {
            case 'nightingale':
                $res = new NightingaleVoice();
                break;
            case 'crow':
                $res = new CrowVoice();
                break;
            default:
                throw new \Exception('Unknown voice type');
        }
        return $res;
    }
}